import { CardEntity } from 'src/cards/entities/card.entity';
import { BaseEntity } from 'src/common/entities/base.entity';
import { UserEntity } from 'src/users/entities/user.entity';
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: "columns" })
export class ColumnEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar')
  title: string;

  @Column()
  userId: UserEntity['id'];

  @OneToMany(() => CardEntity, (card) => card.column, { onDelete: 'CASCADE' })
  cards: CardEntity[];

  @ManyToOne(() => UserEntity, (user) => user.columns)
  user: UserEntity;
}
