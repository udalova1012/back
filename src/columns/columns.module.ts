import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CardsModule } from 'src/cards/cards.module';
import { CardRepository } from 'src/cards/cards.repository';
import { ColumnController } from './columns.controller';
import { ColumnRepository } from './columns.repository';
import { ColumnService } from './columns.service';
import { ColumnEntity } from './entities/column.entity';

@Module({
  providers: [ColumnService],
  controllers: [ColumnController],
  imports: [
    CardsModule,
    TypeOrmModule.forFeature([ColumnRepository, ColumnEntity, CardRepository]),
  ],
})
export class ColumnsModule {}
