import { ColumnEntity } from 'src/columns/entities/column.entity';
import { BaseEntity } from 'src/common/entities/base.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: "cards" })
export class CardEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar')
  columnId: ColumnEntity['id'];

  @Column('varchar')
  text: string;

  @ManyToOne(() => ColumnEntity, (Column) => Column.cards, { onDelete: 'CASCADE' })
  column: ColumnEntity;
}
