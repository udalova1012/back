import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CardsController } from './cards.controller';
import { CardRepository } from './cards.repository';
import { CardService } from './cards.service';
import { CardEntity } from './entities/card.entity';

@Module({
  providers: [CardService],
  exports: [CardService],
  controllers: [CardsController],
  imports: [TypeOrmModule.forFeature([CardEntity, CardRepository])],
})
export class CardsModule {}
