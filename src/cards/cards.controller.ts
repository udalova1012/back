import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiParam, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt.guard';
import { CardService } from './cards.service';
import { CreateCardDto } from './dto/create-card.dto';
import { UpdateCardDto } from './dto/update-card.dto';
import { CardEntity } from './entities/card.entity';

// @UseGuards(JwtAuthGuard)
@ApiTags('cards')
@ApiBearerAuth('JWT')
@Controller('cards')
export class CardsController {
  constructor(private cardService: CardService) {}

  @Get()
  async findAllCards(): Promise<CardEntity[]> {
    return this.cardService.findAll();
  }
  @ApiParam({
    name: 'id',
    type: String,
  })
  @Get(':id')
  async findOneCard(@Param('id') id: CardEntity['id']): Promise<CardEntity> {
    return this.cardService.oneCard(id);
  }

  @ApiBody({
    type: UpdateCardDto,
  })
  @ApiParam({
    name: 'id',
    type: String,
  })
  @Patch(':id')
  async updateCard(
    @Param('id') id: CardEntity['id'],
    @Body() dto: UpdateCardDto,
  ): Promise<CardEntity> {
    return this.cardService.updateCard(id, dto);
  }
  @ApiBody({
    type: CreateCardDto,
  })
  @Post()
  async createCard(@Body() dto: CreateCardDto): Promise<CardEntity> {
    return this.cardService.createCard(dto);
  }
  @ApiParam({
    name: 'id',
    type: String,
  })
  @Delete(':id')
  async delete(@Param('id') id: CardEntity['id']): Promise<boolean> {
    this.cardService.removeCard(id);
    return true;
  }
}
