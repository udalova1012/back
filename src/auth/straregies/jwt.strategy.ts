import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy, StrategyOptions } from "passport-jwt";

import { UserEntity } from "src/users/entities/user.entity";
import { UsersRepository } from "src/users/users.repository";


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly usersRepository: UsersRepository,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: 'secretKey',
      ignoreExpiration: true,
    } as StrategyOptions)
  }

  async validate(payload: { sub: UserEntity['id'] }): Promise<UserEntity> {
    const userId = payload.sub;

    return this.usersRepository.findOneOrFail(userId);
  }
}
