import { ForbiddenException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { compare, hash } from 'bcrypt';
import { UserEntity } from 'src/users/entities/user.entity';
import { UsersRepository } from 'src/users/users.repository';
import { RefreshTokenDto } from './dto/refresh.token.dto';
import { SingInDto } from './dto/signin.dto';
import { SingInResponseDto } from './dto/signin.res.dto';
import { SignUpDto } from './dto/signup.dto';
import { SingUpResponseDto } from './dto/signup.res.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersRepository: UsersRepository,
    private readonly jwtService: JwtService,
  ) {}

  async signUp(dto: SignUpDto): Promise<SingUpResponseDto> {
    const isUserExists = await  this.usersRepository.count({
      where: { email: dto.email }
    });

    if (isUserExists > 0) {
      throw new ForbiddenException('Имя пользователя уже используется');
    }

    const hashedPassword = await hash(dto.password, 10);

    const newUser = await this.usersRepository.save({
      ...dto,
      password: hashedPassword,
    });

    const tokens = await this.createTokenPair(newUser.id);

    return {
      ...tokens,
      userId: this.returnUserFields(newUser),
    };
  }

  async signIn(dto: SingInDto): Promise<SingInResponseDto> {
    
    const user = await this.usersRepository.findOne({
      where: {
        email: dto.email,
      },
    });

    if (!user) {
      throw new ForbiddenException('Неверное имя пользователя');
    }

    await this.verifyPassword(dto.password, user.password);

    const tokens = await this.createTokenPair(user.id);

    return {
      ...tokens,
      userId: this.returnUserFields(user),
    };
  }

  async getNewToken({ refreshToken }: RefreshTokenDto) {
    if (!refreshToken) {
      throw new ForbiddenException('Войдите в систему');
    }

    const result = await this.jwtService.verifyAsync(refreshToken);

    if (!result) {
      throw new ForbiddenException('Токен неверный или время его действия истекло');
    }

    const user = await this.usersRepository.findOne(result.sub);

    const tokens = await this.createTokenPair(user.id);

    return {
      ...tokens,
      userId: this.returnUserFields(user),
    };
  }

  private async verifyPassword(textPassword: UserEntity['password'], hashedPassword: string) {
    const isCorrectPassword = await compare(textPassword, hashedPassword);

    if (!isCorrectPassword) {
      throw new ForbiddenException('Неверный пароль');
    }
  }

  private async createTokenPair(userId: UserEntity['id']) {
    const accessToken = await this.jwtService.signAsync({
      sub: userId,
      expiresIn: '1h',
    })

    const refreshToken = await this.jwtService.signAsync({
      sub: userId,
      expiresIn: '15d',
    })

    return { accessToken, refreshToken }
  }

  private returnUserFields(user: UserEntity) {
    return user.id
  }

}
