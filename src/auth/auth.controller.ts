import { Body, Controller, Post } from '@nestjs/common';
import { ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { RefreshTokenDto } from './dto/refresh.token.dto';
import { SingInDto } from './dto/signin.dto';
import { SingInResponseDto } from './dto/signin.res.dto';
import { SignUpDto } from './dto/signup.dto';
import { SingUpResponseDto } from './dto/signup.res.dto';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiBody({
    type: SignUpDto,
  })
  @ApiResponse({
    type: SingUpResponseDto
  })
  @Post('sign-up')
  async signUp(@Body() signInDto: SignUpDto): Promise<SingUpResponseDto> {
    return this.authService.signUp(signInDto);
  }

  @ApiBody({
    type: SingInDto,
  })
  @ApiResponse({
    type: SingInResponseDto
  })
  @Post('sign-in')
  async singIn(@Body() singInDto: SingInDto): Promise<SingInResponseDto> {
    return this.authService.signIn(singInDto);
  }

  @Post('sign-in/access-token')
  async getNewToken(@Body() refreshTokenDto: RefreshTokenDto) {
    return this.authService.getNewToken(refreshTokenDto);
  }
}
